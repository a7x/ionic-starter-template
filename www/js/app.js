// Ionic Starter App

angular.module('starter', ['ionic', 'starter.controllers', 'ionic.contrib.drawer'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.markup-list', {
    url: '/markup-list',
    views: {
      'menuContent': {
        templateUrl: 'templates/_markup-list.html'
      }
    }
  })

  .state('app.markup', {
    url: '/markup',
    views: {
      'menuContent': {
        templateUrl: 'templates/_markup.html'
      }
    }
  });


  $urlRouterProvider.otherwise('/app/markup');
});
